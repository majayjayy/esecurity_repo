-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2018 at 12:23 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esecurity`
--

-- --------------------------------------------------------

--
-- Table structure for table `qrcode`
--

CREATE TABLE `qrcode` (
  `qrcode_id` int(3) UNSIGNED NOT NULL,
  `userdetails_id` int(3) UNSIGNED NOT NULL,
  `url` varchar(50) NOT NULL,
  `no_of_entries` int(1) UNSIGNED NOT NULL,
  `used_entries` int(1) UNSIGNED NOT NULL,
  `valid_up_to` date DEFAULT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `userdetails_id` int(3) UNSIGNED NOT NULL,
  `user_group_id` int(1) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile_no` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`userdetails_id`, `user_group_id`, `name`, `email_id`, `password`, `mobile_no`) VALUES
(1, 2, 'priya', 'priya@gmail.com', 'priya', '9876543210'),
(2, 2, 'farha', 'farhatahseen5@gmail.com', 'farha', '9876556789');

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `user_group_id` int(1) UNSIGNED NOT NULL,
  `type_of_user` varchar(30) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usergroups`
--

INSERT INTO `usergroups` (`user_group_id`, `type_of_user`, `date_created`) VALUES
(1, 'admin', '2018-05-23 06:19:54'),
(2, 'owner', '2018-05-23 06:20:25'),
(3, 'security', '2018-05-23 06:20:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `qrcode`
--
ALTER TABLE `qrcode`
  ADD PRIMARY KEY (`qrcode_id`),
  ADD UNIQUE KEY `userdetails_id` (`userdetails_id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`userdetails_id`),
  ADD UNIQUE KEY `email_id` (`email_id`),
  ADD KEY `user_group_id` (`user_group_id`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`user_group_id`),
  ADD UNIQUE KEY `type_of_user` (`type_of_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `qrcode`
--
ALTER TABLE `qrcode`
  MODIFY `qrcode_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `userdetails_id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `user_group_id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `qrcode`
--
ALTER TABLE `qrcode`
  ADD CONSTRAINT `qrcode_ibfk_1` FOREIGN KEY (`userdetails_id`) REFERENCES `userdetails` (`userdetails_id`);

--
-- Constraints for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD CONSTRAINT `userdetails_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `usergroups` (`user_group_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


CREATE TABLE IF NOT EXISTS AuthDetails (
uid INT(1) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
device_id VARCHAR(50) NOT NULL, 
auth_key VARCHAR(500) NOT NULL, 
UNIQUE `info` (`uid`, `device_id`, `auth_key`)
);

INSERT INTO `AuthDetails` (`uid`, `device_id`, `auth_key`) VALUES
(1, 'POSTMAN@6789', '3HoHfQXCUojwALqTGXt3p2LByqPLYUbRsqox8cZSq0eRLvAhjTayLL102FKQqMxI3M6ZOVH6VMMw8dDgXDFyfvx729NhLaQDNMss');