<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

// include database and object files

class Companies{
 
    // database connection and table name
    private $conn;
    private $companies_table = "companies";

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function companyCheck($company_name) {
        $company_id = null;
        // check if company name exists
        $company_name_query = "SELECT * from " .$this->companies_table. " WHERE company_name = '".$company_name . "'";
        $stmt = $this->conn->prepare($company_name_query);
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num == 1) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
            }
        } else {
            $sql = "INSERT into ".$this->companies_table." (company_name) values('".$company_name."');";
            if($this->conn->exec($sql)) {
                $company_id = $this->conn->lastInsertId();
            }
        }
        return $company_id;
    }

    public function returnCompanyName($company_id) {
        $company_name = null;
        // check if company name exists
        $company_id_query = "SELECT * from " .$this->companies_table. " WHERE company_id = '".$company_id . "'";
        $stmt = $this->conn->prepare($company_id_query);
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num == 1) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
            }
        }
        return $company_name;
    }

    public function returnCompanyId($company_name) {
        $company_id = null;
        // check if company name exists
        $company_name_query = "SELECT * from " .$this->companies_table. " WHERE company_name = '".$company_name . "'";
        $stmt = $this->conn->prepare($company_name_query);
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num == 1) {
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
            }
        }
        return $company_id;
    }

}

?>