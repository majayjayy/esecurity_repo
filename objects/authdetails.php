<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

// include database and object files
include_once '../objects/utils.php';

class AuthDetails{
 
    // database connection and table name
    private $conn;
    private $auth_table_name = "authdetails";
 
    // object properties
    public $dev_username;
    public $dev_pass;
    public $device_id;
    private $username = "dev_esecurityapp";
    private $pass = "8CEAF51C7C292";

    private $length=100;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function authorize(){
        $data=array();
        $utils = new Utils($this->conn);

        if ($this->dev_username == $this->username && $this->dev_pass == $this->pass) {
            
            // Checking if user has already logged in earlier
            $query = "SELECT * FROM " . $this->auth_table_name . " WHERE device_id = '" .$this->device_ID. "'";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $num = $stmt->rowCount();

            if($num == 1) {
                
                // Existing user
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                extract($row);
                $temp1 = $utils->statusDefaultMessage("1", "Success!");
                $temp2 = array(
                    "data" => array(
                        "device_ID" => $device_id,
                        "activation_key" => $auth_key,
                        "status" => $status
                    )
                );
                $data = array_merge($temp1, $temp2);
            } else {

                // Create new activation key for user
                $newActivationKey = $utils->randomPassword($this->length);

                // Checking duplication of activation key
                $query = "SELECT * FROM " . $this->auth_table_name . " WHERE auth_key = '" .$newActivationKey;
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                $num = $stmt->rowCount();

                if($num == 1) { // Existing user
                    $newActivationKey = $utils->randomPassword($this->length);
                }

                // Inserting activationkey for new user
                $sql = "INSERT into ".$this->auth_table_name." (device_id, auth_key) values('".$this->device_ID."', '".$newActivationKey."');";
                $this->conn->exec($sql);

                $temp1 = $utils->statusDefaultMessage("1", "Success!");
                $temp2 = array(
                    "data" => array(
                        "device_ID" => $this->device_ID,
                        "activation_key" => $newActivationKey
                    )
                );
                $data = array_merge($temp1, $temp2);
            }
        } else {

            $data = $utils->statusDefaultMessage("0", "Security breach!");

        }

        return $data;

    }

}

?>