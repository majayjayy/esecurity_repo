<?php

error_reporting(E_ALL); 
ini_set('display_errors', 1);

class Utils{
 
    // database connection and table name
    private $conn;
    private $auth_table_name = "authdetails";
    private $userdetails_table = "userdetails";

    // object properties
    const admin_group_id = 1;
    const owner_group_id = 2;
    const security_group_id = 3;
    const visitor_group_id = 4;

    const inactive_status = 0;
    const active_status = 1;
 
    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    public function securityCheck($authKey) {
        $data=array();
        // Security check with Activation key
        $activat_query = "SELECT * from " .$this->auth_table_name. " WHERE auth_key = '".$authKey . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($activat_query);
        // execute query
        $stmt->execute();
        $num = $stmt->rowCount();
        if ($num != 1) {
            $data = $this->statusDefaultMessage("0", "Security check failed!");
            $securityStatus = new Status($data, false);
        } else {
            $securityStatus = new Status($data, true);
        }
        return $securityStatus;
    }

    public function randomPassword($length) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function validateEmail($emailId) {
        $data=array();
        // check if e-mail address is well-formed
        if (!filter_var($emailId, FILTER_VALIDATE_EMAIL)) {
            $data = $this->statusDefaultMessage("0", "Invalid email format!");
            $emailStatus = new Status($data, false);
        } else {
            $emailStatus = new Status($data, true);
        }
        return $emailStatus;
    }

    public function validateMobileNumber($mobileNo){
        $data=array();
        // check if mobile number is valid (10 digits)
        if (!is_numeric($mobileNo)||!preg_match('/[0-9]{10}+$/',$mobileNo)) {
            $data = $this->statusDefaultMessage("0", "Please enter a valid mobile number!");
            $mobileNumberStatus = new Status($data, false);
        } else {
            $mobileNumberStatus = new Status($data, true);
        }
        return $mobileNumberStatus;
    }

    public function usernameDuplication($username) {
        $data=array();
        // check for email validity
        $username_query = "SELECT * from " .$this->userdetails_table. " WHERE email_id = '".$username . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($username_query);
        // execute query
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num == 1) {
            $data = $this->statusDefaultMessage("0", "Username already exists. Please choose another username!");
            $duplicationStatus = new Status($data, false);
        } else {
            $duplicationStatus = new Status($data, true);
        }
        return $duplicationStatus;
    }

    public function mobileNumberDuplication($mobile_no) {
        $data=array();
        // check for email validity
        $username_query = "SELECT * from " .$this->userdetails_table. " WHERE mobile_no = '".$mobile_no . "'";
        // prepare query statement
        $stmt = $this->conn->prepare($username_query);
        // execute query
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num == 1) {
            $data = $this->statusDefaultMessage("0", "Mobile number already exists. Please enter alternate number!");
            $duplicationStatus = new Status($data, false);
        } else {
            $duplicationStatus = new Status($data, true);
        }
        return $duplicationStatus;
    }

    public function statusDefaultMessage($code, $msg) {
        $data=array();
        $data = array(
                "response_code" => $code,
                "status" => $msg
            );
        return $data;
    }

}

class Status {
    
    private $msg;
    private $isValid;

    public function __construct($msg, $isValid)
    {
        $this->msg = $msg;
        $this->isValid = $isValid;
    }

    public function getMessage()
    {
        return $this->msg;
    }

    public function isValid()
    {
        return $this->isValid;
    }

    public function setMessage($msg)
    {
        $this->msg=$msg;
    }

    public function setStatus($isValid)
    {
        $this->isValid=$isValid;
    }

}

?>