<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

// include database and object files
include_once '../objects/utils.php';
include_once '../objects/companies.php';

class UserDetails{
 
    // database connection and table name
    private $conn;
    private $userdetails_table = "userdetails";
    private $companies_table = "companies";

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function insertOwner($user_group_id, $name, $email_id, $enc_pass, $mobile_no, $company_name, $created_by) {
        $data=array();
        $utils = new Utils($this->conn);
        $companies = new Companies($this->conn);
        $company_id = null;
        if ($company_name != null || $company_name != "") {
            $company_id = $companies->companyCheck($company_name);
        }
        if ($created_by==null && ($user_group_id==Utils::admin_group_id || $user_group_id==Utils::owner_group_id)) {
            // query to insert record
            $sql = "INSERT into ".$this->userdetails_table." (user_group_id, name, email_id, password, mobile_no, company_id) values(".$user_group_id.", '".$name."', '".$email_id."', '".$enc_pass."', '".$mobile_no."', '".$company_id."');";
            if($this->conn->exec($sql)){
                $data = $utils->statusDefaultMessage("1", "Signup successful!");
            } else {
                $data = $utils->statusDefaultMessage("0", "Signup failed!");
            }
            return $data;
        } else if($created_by!=null && ($user_group_id==Utils::owner_group_id)) {
            $validity_query = "SELECT company_id from " .$this->userdetails_table. " WHERE user_details_id = '".$created_by . "' AND (user_group_id = '".Utils::admin_group_id . "' OR user_group_id = '".Utils::owner_group_id . "')";
            $stmt = $this->conn->prepare($validity_query);
            // execute query
            $stmt->execute();
            $num = $stmt->rowCount();
            if ($num==1) {
                if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    // query to insert record
                    $sql = "INSERT into ".$this->userdetails_table." (user_group_id, name, email_id, password, mobile_no, company_id, created_by, status) values(".$user_group_id.", '".$name."', '".$email_id."', '".$enc_pass."', '".$mobile_no."', '".$company_id."', '".$created_by."', '".Utils::active_status."');";
                    if($this->conn->exec($sql)){
                        $data = $utils->statusDefaultMessage("1", "New owner added!");
                    } else {
                        $data = $utils->statusDefaultMessage("0", "Unable to add new owner!");
                    }
                } else {
                    $data = $utils->statusDefaultMessage("0", "Unable to fetch!");
                }
            } else {
                $data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");
            }
            return $data;
        } else {
            $data = $utils->statusDefaultMessage("0", "Signup failed!");
        }
        return $data;
    }

    public function insertSecurity($user_group_id, $name, $enc_pass, $mobile_no, $created_by) {
        $data=array();
        $utils = new Utils($this->conn);
        if ($created_by==null && ($user_group_id==Utils::security_group_id)) {
            // query to insert record
            $sql = "INSERT into ".$this->userdetails_table." (user_group_id, name, password, mobile_no) values(".$user_group_id.", '".$name."', '".$enc_pass."', '".$mobile_no."');";
            if($this->conn->exec($sql)){
                $data = $utils->statusDefaultMessage("1", "Signup successful!");
            } else {
                $data = $utils->statusDefaultMessage("0", "Signup failed!");
            }
            return $data;
        } else if ($created_by!=null && ($user_group_id==Utils::security_group_id)) {
            $validity_query = "SELECT * from " .$this->userdetails_table. " WHERE user_details_id = '".$created_by . "' AND user_group_id = '".Utils::admin_group_id . "'";
            $stmt = $this->conn->prepare($validity_query);
            // execute query
            $stmt->execute();
            $num = $stmt->rowCount();
            if ($num==1) {
                // query to insert record
                $sql = "INSERT into ".$this->userdetails_table." (user_group_id, name, password, mobile_no, created_by, status) values(".$user_group_id.", '".$name."', '".$enc_pass."', '".$mobile_no."', '".$created_by."', '".Utils::active_status."');";
                if($this->conn->exec($sql)){
                    $data = $utils->statusDefaultMessage("1", "New security added!");
                } else {
                    $data = $utils->statusDefaultMessage("0", "Unable to add new security!");
                }
            } else {
                $data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");
            }
            return $data;
        } else {
            $data = $utils->statusDefaultMessage("0", "Signup failed!");
        }
        return $data;
    }

    public function insertVisitor($user_group_id, $name, $email_id, $mobile_no, $vehicle_id, $created_by) {
        $data=array();
        $utils = new Utils($this->conn);
        if ($created_by!=null && $user_group_id==Utils::visitor_group_id) {
            $validity_query = "SELECT company_id from " .$this->userdetails_table. " WHERE user_details_id = '".$created_by . "' AND (user_group_id = '".Utils::admin_group_id . "' OR user_group_id = '".Utils::owner_group_id . "')";
            $stmt = $this->conn->prepare($validity_query);
            // execute query
            $stmt->execute();
            $num = $stmt->rowCount();
            if ($num==1) {
                if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    // query to insert record
                    $sql = "INSERT into ".$this->userdetails_table." (user_group_id, name, email_id, mobile_no, company_id, vehicle_id, created_by, status) values(".$user_group_id.", '".$name."', '".$email_id."', '".$mobile_no."', ".$company_id.", '".$vehicle_id."', '".$created_by."', '".Utils::active_status."');";
                    if($this->conn->exec($sql)){
                        $data = $utils->statusDefaultMessage("1", "New visitor added!");
                    } else {
                        $data = $utils->statusDefaultMessage("0", "Unable to add visitor!");
                    }
                }
            } else {
                $data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");
            }
        } else {
            $data = $utils->statusDefaultMessage("0", "Signup failed!");
        }
        return $data;
    }

    public function userLogin($user_group_id, $email_id, $mobile_no, $enc_pass) {
        $data=array();
        $utils = new Utils($this->conn);
        if ($mobile_no==null && $user_group_id==Utils::owner_group_id) {
            // select user query
            $login_query = "SELECT * FROM " . $this->userdetails_table . " WHERE email_id = '" .$email_id. "' and password = '" .$enc_pass. "'";
            // prepare query statement
            $stmt = $this->conn->prepare($login_query);
            // execute query
            $stmt->execute();
            $num = $stmt->rowCount();
            if($num == 1) {
                // retrieve our table contents
                // fetch() is faster than fetchAll()
                // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
                if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['name'] to
                    // just $name only
                    extract($row);
                    $temp1 = $utils->statusDefaultMessage("1", "Success!");
                    $temp2 = array(
                        "data" => array(
                            "user_details_id" => $user_details_id,
                            "user_group_id" => $user_group_id,
                            "name" => $name,
                            "email_id" => $email_id,
                            "mobile_no" => $mobile_no,
                            "company_id" => $company_id,
                            "date_created" => $date_created,
                            "date_updated" => $date_updated,
                            "created_by" => $created_by,
                            "status" => $status
                        )
                    );
                    $data = array_merge($temp1, $temp2);
                }
            } else {
                $data = $utils->statusDefaultMessage("0", "Unable to login! Check whether login ID & password are correct");
                echo json_encode($data);
                exit();
            }
        } else if ($email_id==null && $user_group_id==Utils::security_group_id) {
            // select user query
            $login_query = "SELECT * FROM " . $this->userdetails_table . " WHERE mobile_no = '" .$mobile_no. "' and password = '" .$enc_pass. "'";
            // prepare query statement
            $stmt = $this->conn->prepare($login_query);
            // execute query
            $stmt->execute();
            $num = $stmt->rowCount();
            if($num == 1) {
                // retrieve our table contents
                // fetch() is faster than fetchAll()
                // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
                if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['name'] to
                    // just $name only
                    extract($row);
                    $temp1 = $utils->statusDefaultMessage("1", "Success!");
                    $temp2 = array(
                        "data" => array(
                            "user_details_id" => $user_details_id,
                            "user_group_id" => $user_group_id,
                            "name" => $name,
                            "mobile_no" => $mobile_no,
                            "date_created" => $date_created,
                            "date_updated" => $date_updated,
                            "created_by" => $created_by,
                            "status" => $status
                        )
                    );
                    $data = array_merge($temp1, $temp2);
                }
            } else {
                $data = $utils->statusDefaultMessage("0", "Unable to login! Check whether login ID & password are correct");
                echo json_encode($data);
                exit();
            }
        } else {
            $data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");
        }
        return $data;
    }

    public function updateUser($user_id, $name, $email_id, $mobile_no, $vehicle_id){
        $data=array();
        $utils = new Utils($this->conn);
        // update query
        $update_query = "UPDATE " . $this->userdetails_table . " SET name = '" . $name . "', email_id = '" . $email_id . "', mobile_no = '" . $mobile_no . "', vehicle_id = '" . $vehicle_id . "', date_updated = now() WHERE user_details_id = " . $user_id;
        $stmt = $this->conn->prepare($update_query);
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num == 1) {
            $select_query = "SELECT * FROM " . $this->userdetails_table . " WHERE user_details_id = " .$user_id;
            $stmt = $this->conn->prepare($select_query);
            $stmt->execute();
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $temp1 = $utils->statusDefaultMessage("1", "User updated!");
                $temp2 = array(
                    "data" => array(
                        "user_details_id" => $user_details_id,
                        // "user_group_id" => $user_group_id,
                        "name" => $name,
                        "email_id" => $email_id,
                        "mobile_no" => $mobile_no,
                        "vehicle_id" => $vehicle_id,
                        // "date_created" => $date_created,
                        "date_updated" => $date_updated,
                        // "created_by" => $created_by,
                        // "status" => $status
                    )
                );
                $data = array_merge($temp1, $temp2);
            } else {
                $data = $utils->statusDefaultMessage("0", "User updated, unable to fetch data!");
            }
        } else {
            $data = $utils->statusDefaultMessage("0", "User not updated!");
        }
        return $data;
    }

    public function viewUserGroup($usergroup_id, $view_group_id, $userdetails_id, $companyId) {
        $data=array();
        $data["data"]=array();
        $utils = new Utils($this->conn);
        $companies = new Companies($this->conn);
        $view_query = "SELECT * FROM " . $this->userdetails_table . " WHERE user_group_id = " .$view_group_id;
        $stmt = $this->conn->prepare($view_query);
        $stmt->execute();
        $num = $stmt->rowCount();
        if($num > 0) {
            $count=0;
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                if ($usergroup_id == Utils::admin_group_id || ($usergroup_id == Utils::owner_group_id && ($created_by == $userdetails_id || $company_id == $companyId))) {
                    $count++;
                    $temp2 = array(
                        "user_details_id" => $user_details_id,
                        "user_group_id" => $user_group_id,
                        "name" => $name,
                        "email_id" => $email_id,
                        "mobile_no" => $mobile_no,
                        "company_name" => $companies->returnCompanyName($company_id),
                        "vehicle_id" => $vehicle_id,
                        "date_created" => $date_created,
                        "date_updated" => $date_updated,
                        "created_by" => $created_by,
                        "status" => $status
                    );
                    array_push($data["data"], $temp2);
                }
                $temp1 = $utils->statusDefaultMessage("1", "Success!");
                $data = array_merge($temp1, $data);
                $data = array_merge($data, array("count" => $count));
            }
        }
        if ($count==0) {
            $data = $utils->statusDefaultMessage("0", "No results!");
        }
        return $data;
    }

}

?>