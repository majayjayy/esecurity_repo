<?php
class Database{
 
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "esecurity";
    private $username = "root";
    private $password = "";
    public $conn;
 
    // get the database connection
    public function getConnection(){
 
        $this->conn = null;
 
        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }
        catch(PDOException $exception){

            $code="0";
            $msg = "Connection error: " . $exception->getMessage();

            $data= array(
                "response_code" => $code,
                "status: " => $msg
            );

            echo json_encode($data);

        }
 
        return $this->conn;
    }
}

// include_once '../objects/commonfuncs.php';

// class Database{
 
//     // specify your own database credentials
//     private $host = "localhost";
//     private $db_name = "esecurit";
//     private $username = "root";
//     private $password = "";
//     public $conn;
 
//     // get the database connection
//     public function getConnection(){
 
//         $this->conn = null;
 
//         try {
//             $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
//             $this->conn->exec("set names utf8");
//         }
//         catch(PDOException $exception){

//             $data=array();

//             $commonfuncs = new CommonFunctions($db);
//             $data = $commonfuncs->statusDefaultMessage("1", "Connection error: " . $exception->getMessage());

//             echo json_encode($data);

//         }
 
//         return $this->conn;
//     }
// }
?>