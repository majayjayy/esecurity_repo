<?php
error_reporting(E_ALL);
ini_set("display_errors",0);
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/utils.php';
include_once '../objects/userdetails.php';

// instantiate database and product object
$database = new Database();	
$db = $database->getConnection();

$utils = new Utils($db);
$userdetails = new UserDetails($db);

// Table Initialization
$userdetails_table = "userdetails";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// set value to object property
$data = array();
$user_group_id = isset($_POST['user_group_id']) ? $_POST['user_group_id']:"";
$email_id = isset($_POST['email_id']) ? $_POST['email_id']:"";
$mobile_no = isset($_POST['mobile_no']) ? $_POST['mobile_no']:"";
$password = isset($_POST['password']) ? $_POST['password']:"";
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";
$enc_pass = md5($password);
$status = isset($_POST['status']) ? $_POST['status']:"0";

//start
if ($mobile_no == null || $mobile_no == "") {
	if($_SERVER["REQUEST_METHOD"] != "POST" || $email_id == null || $email_id == "" || $password == null || $password == "" || $activ_key == null || $activ_key == ""){

		$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
		echo json_encode($data);
		exit();

	} 

	$emailStatus = $utils->validateEmail($email_id);
	$securityCheckStatus = $utils->securityCheck($activ_key);

	if(!$emailStatus->isValid()) {
		$data = $emailStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if(!$securityCheckStatus->isValid()) {
		$data = $securityCheckStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	$data = $userdetails->userLogin($user_group_id, $email_id, $mobile_no, $enc_pass);

} elseif ($email_id == null || $email_id == "") {
	if($_SERVER["REQUEST_METHOD"] != "POST" || $mobile_no == null || $mobile_no == "" || $password == null || $password == "" || $activ_key == null || $activ_key == ""){

		$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
		echo json_encode($data);
		exit();

	} 

	$mobileNumberStatus = $utils->validateMobileNumber($mobile_no);
	$securityCheckStatus = $utils->securityCheck($activ_key);

	if (!$mobileNumberStatus->isValid()) {
		$data = $mobileNumberStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if(!$securityCheckStatus->isValid()) {
		$data = $securityCheckStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	$data = $userdetails->userLogin($user_group_id, $email_id, $mobile_no, $enc_pass);

} else {

	$data = $utils->statusDefaultMessage("0", "Something went wrong!");	

}

echo json_encode($data);

?>