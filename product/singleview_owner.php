<?php
error_reporting(E_ALL);
ini_set("display_errors",0);
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';

// instantiate database and product object
$database = new Database();	
$db = $database->getConnection();

// Table Initialization
$userdetails_table="userdetails";
$auth_table="authdetails";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// check connection to database
if (!$db) {
	die("Connection failed: " . mysqli_connect_error());
	exit;
}

$data = array();
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";
$owner_id=isset($_POST['owner_id']) ? $_POST['owner_id']:"";
$user_id=isset($_POST['user_id'])?$_POST['user_id']:"";

if($_SERVER["REQUEST_METHOD"] != "POST" ||$activ_key == null || $activ_key == ""||$user_id==null||$user_id==""||$owner_id==""||$owner_id==null){

	$code="0";
	$msg = "Incomplete Details!"; 

	$data= array(
		"response_code" => $code,
		"status" => $msg
	);

} else {
	$activat_query = "SELECT * from " .$auth_table. " WHERE auth_key = '".$activ_key . "'";
	$stmt = $db->prepare($activat_query);
	// execute query
	$stmt->execute();
	$num = $stmt->rowCount();

	if ($num != 1) {

			$code="0";
			$msg="Security check failed!";

			$data = array(
			"response_code" => $code,
			"status" => $msg
		);

	} else {
			
			$groupid_query="select user_group_id from ".$userdetails_table." where user_details_id=".$user_id;
			$stmt=$db->prepare($groupid_query);
			$stmt->execute();
			$num=$stmt->rowCount();
			if($num!=1)
			{
				$code="0";
					$msg="Details not found!";

					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
			}
		
			else
			{
				if($row=$stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row);
		
					//Admin and owner viewing details.. $user_group_id - id of viewing user 
					if(($user_group_id==1)||($user_group_id==2&&$user_id==$owner_id))
					{	//Admin viewing owner details
						$query="select * from ".$userdetails_table." where user_details_id=".$owner_id;
						$stmt = $db->prepare($query);
						$stmt->execute();
						$num = $stmt->rowCount();

						if ($num != 1) {

							$code="0";
							$msg="No user found!";

							$data = array(
							"response_code" => $code,
							"status" => $msg
							);

						} else {
			
							if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
							extract($row);
			  
							if($row['user_group_id']==2)
							{	
									$comp_query="select company_name from companies where company_id=".$company_id;
									$stmt1 = $db->prepare($comp_query);
									$stmt1->execute();
									if($row1=$stmt1->fetch(PDO::FETCH_ASSOC))
									{extract($row1);
					
									$code='1';
									$msg="success";
									$data= array(
									"response_code" => $code,
									"status" => $msg,
									"data" =>$product_item=array(
									"name" => $name,
									"email_id" => $email_id,
									"mobile_no" => $mobile_no,
									"company_id"=>$company_id,
									"company_name"=>$company_name,
									"date_created"=>$date_created
							
										)
					
									);
			  
									}else
									{	$code="0";
										$msg="Details not found!";
	
												$data = array(
												"response_code" => $code,
												"status" => $msg
												);
									}
								
							}
						else{
							$code="0";
							$msg="Not an owner!";

							$data = array(
										"response_code" => $code,
										"status" => $msg
								);
							}			
			  
			
			
					}else{
						
						$code="0";
					$msg="Details not found!";

					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
			   }
		
	
		}
	
	}	
	else{
					$code="0";
					$msg="Not an authorized user group for viewing details!";

					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
	}
	}
	else{}
}
	}
}

echo json_encode($data);
?>