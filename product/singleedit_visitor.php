<?php
error_reporting(E_ALL);
ini_set("display_errors",0);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

// assign necessary tables
$userdetails_table = "userdetails";
$auth_table = "authdetails";
$companies_table="companies";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// check connection to database
if (!$db) {
	die("Connection failed: " . mysqli_connect_error());
	exit;
}

$data = array();
$user_id = isset($_POST['user_id']) ? $_POST['user_id']:"";
$visitor_id=isset($_POST['visitor_id']) ? $_POST['visitor_id']:"";
$name = isset($_POST['name']) ? $_POST['name']:"";
$mobile_no = isset($_POST['mobile_no']) ? $_POST['mobile_no']:"";
$company_name=isset($_POST['company_name'])?$_POST['company_name']:"";
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";
$vehicle_id=isset($_POST['vehicle_id'])?$_POST['vehicle_id']:"";


//start
if($_SERVER["REQUEST_METHOD"] != "POST" || $activ_key == null || $activ_key == ""||$visitor_id==""||$visitor_id==null||$name==""||$name==null||$mobile_no==""||$mobile_no==null){

	$code="0";
	$msg = "Invalid parameters!"; 

	$data= array(
		"response_code" => $code,
		"status" => $msg
	);

} else {

	// Security check with Activation key
	$activat_query = "SELECT * from " .$auth_table. " WHERE auth_key = '".$activ_key . "'";

	// prepare query statement
	$stmt = $db->prepare($activat_query);
	// execute query
	$stmt->execute();
	$num = $stmt->rowCount();

	if ($num != 1) {

		$code="0";
		$msg="Security check failed!";

		$data = array(
			"response_code" => $code,
			"status" => $msg
		);

	} else {
		
			$groupid_query="select * from ".$userdetails_table." where user_details_id=".$user_id;
			$stmt=$db->prepare($groupid_query);
			$stmt->execute();
			$num=$stmt->rowCount();
			if($num!=1)
			{
				$code="0";
					$msg="Details not found!";

					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
			}
		
			else
			{
				if($row=$stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row);
					$company_id_user=$company_id;
	
					if($user_group_id==1)
					{	
						//Admin editing the details of the visitor
						$user_query="select user_group_id from userdetails where user_details_id=".$visitor_id;
						$stmt1 = $db->prepare($user_query);
						$stmt1->execute();
						if($row1=$stmt1->fetch(PDO::FETCH_ASSOC)){
							extract($row1);
							$visitor_group_id=$user_group_id;
							if($visitor_group_id==4)
							{	
								$company_name_query = "SELECT * from " .$companies_table. " WHERE company_name = '".$company_name . "'";
								// prepare query statement
								$stmt = $db->prepare($company_name_query);
								// execute query
								$stmt->execute();
								$num = $stmt->rowCount();
								if($num == 1) {
									if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
										extract($row);
										$sql_sub = "UPDATE ".$userdetails_table." SET ";
										$sql_sub = $sql_sub. "name='".$name."', mobile_no='".$mobile_no."',company_id=".$company_id;
										$sql_sub=$sql_sub. " WHERE user_details_id=".$visitor_id.";";
										$sql=$sql_sub;
										if($db->exec($sql)){

											$code="1";
											$msg="Details updated successfully!";

											$data= array(
											"response_code" => $code,
											"status" => $msg
											);
										} else {

													$code="0";
													$msg="Details could not be updated 1!";

													$data= array(
													"response_code" => $code,
													"status" => $msg
													);
												}
									}else
									{
										$code="0";
										$msg="Details not found!";

										$data= array(
										"response_code" => $code,
										"status" => $msg
											);
									}	
							}
						else{
						$sql = "INSERT into ".$companies_table." (company_name) values('".$company_name."');";
						if($db->exec($sql)) {
							$last_id = $db->lastInsertId();
							$sql_sub = "UPDATE ".$userdetails_table." SET ";
							$sql_sub = $sql_sub. "name='".$name."', mobile_no=".$mobile_no.",company_id=".$last_id;
							$sql_sub=$sql_sub. " WHERE user_details_id=".$visitor_id.";";
							$sql=$sql_sub;
							if($db->exec($sql)){

							$code="1";
							$msg="Details updated successfully!";

							$data = array(
								"response_code" => $code,
								"status" => $msg
							   );

							} else {

							$code="0";
							$msg="Details couldn't be updated!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);

							}
						}
					else
					{
							$code="0";
							$msg="Details couldn't be updated!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
					}
				}
		
			}
			else{
			
							$code="0";
							$msg="Not a visitor";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
				}	
			}
					else{}
	}
					
			else if($user_group_id==2)
		{		
							//Details of visitor to valid whether their company id's match or not
							$query="select * from ".$userdetails_table." where user_details_id=".$visitor_id;
							$stmt = $db->prepare($query);
			
							// execute query
							$stmt->execute();
							$num = $stmt->rowCount();

							if ($num != 1) {

									$code="0";
									$msg="No results found for the specified visitor !";

										$data = array(
										"response_code" => $code,
									"status" => $msg
									);

							}
							else {
			
								if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
									// extract row
									
									$company_id_before=$row['company_id'];
									$visitor_group_id=$row['user_group_id'];
									if($visitor_group_id==4&&$company_id_before==$company_id_user)
									{
										$company_name_query = "SELECT * from " .$companies_table. " WHERE company_name = '".$company_name."'";
										// prepare query statement
										$stmt = $db->prepare($company_name_query);
										// execute query
										$stmt->execute();
										$num = $stmt->rowCount();
										if($num == 1) {
												if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
												extract($row);
												$company_id_visitor=$company_id;
												$sql_sub = "UPDATE ".$userdetails_table." SET ";
												$sql_sub = $sql_sub."name='".$name."',mobile_no='".$mobile_no."',company_id=".$company_id_visitor;
												$sql_sub=$sql_sub." WHERE user_details_id=".$visitor_id.";";
												$sql=$sql_sub;

													//update
												if($db->exec($sql)){

													$code="1";
													$msg="Details updated successfully!";

													$data= array(
													"response_code" => $code,
													"status" => $msg
													);
												} else {

														$code="0";
														$msg="Details could not be updated !";

														$data= array(
														"response_code" => $code,
														"status" => $msg
															);
													}
												}
											else
										{
												$code="0";
												$msg="Details not found";

												$data= array(
												"response_code" => $code,
												"status" => $msg
												);

		
										}	
									}
								else{
										$sql = "INSERT into ".$companies_table." (company_name) values('".$company_name."');";
										if($db->exec($sql)) {
											$last_id = $db->lastInsertId();
											$sql_sub = "UPDATE ".$userdetails_table." SET ";
											$sql_sub = $sql_sub. "name='".$name."', mobile_no='".$mobile_no."',company_id=".$last_id;
											$sql_sub=$sql_sub. " WHERE user_details_id=".$visitor_id.";";
											$sql=$sql_sub;
											if($db->exec($sql)){

												$code="1";
												$msg="Details updated successfully!";

												$data = array(
												"response_code" => $code,
												"status" => $msg
												);

											} else {

												$code="0";
												$msg="Details couldn't be updated!";

												$data = array(
												"response_code" => $code,
												"status" => $msg
													);

											}
									}
									else
									{
										$code="0";
										$msg="Details couldn't be updated!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
									}
								}
										
										
										
										
									}
									else{
										
									$code="0";
										$msg="Not an authorized person/Not a visitor!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);	
										
									}
								}
								else {
									
										$code="0";
										$msg="Details not found!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
									
									);
								}
							}
		}
				
				
		
		else
		{
		$code="0";
			$msg="You are not an authorized user!";

			$data= array(
				"response_code" => $code,
				"status" => $msg
			);

		
		}
	}
}
	}
}

echo json_encode($data);
//end
?>