<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$userdetails_table_name = "userdetails";
$qrCode_table_name = "qrcode";
$auth_table="authdetails";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// check connection to database
if (!$db) {
	die("Connection failed: " . mysqli_connect_error());
	exit;
}

$data = array();
$userdetails_id = isset($_POST['userdetails_id']) ? $_POST['userdetails_id']:"";
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";

//start
if($_SERVER["REQUEST_METHOD"] != "POST" || $activ_key == null || $activ_key == ""){

	$code="0";
	$msg = "Invalid parameters!"; 

	$data= array(
		"response_code" => $code,
		"status" => $msg
	);

} else {

	// Security check with Activation key
	$activat_query = "SELECT * from " .$auth_table. " WHERE auth_key = '".$activ_key . "'";

	// prepare query statement
	$stmt = $db->prepare($activat_query);
	// execute query
	$stmt->execute();
	$num = $stmt->rowCount();

	if ($num != 1) {

		$code="0";
		$msg="Security check failed!";

		$data = array(
			"response_code" => $code,
			"status" => $msg
		);

	} else {

		// select all query
		$query = "SELECT * FROM " . $userdetails_table_name . " WHERE userdetails_id = '" .$userdetails_id. "'";
		$stmt = $db->prepare($query);
		$stmt->execute();
		$num = $stmt->rowCount();

		if($num == 1) {

		    // products array
		    $products_arr=array();
		    $response_arr=array();

		    
		    if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		        // extract row
		        // this will make $row['name'] to
		        // just $name only
		        extract($row);
				
				if($user_group_id==2){
				
					$url = isset($_POST['url']) ? $_POST['url']:"";
					$no_of_entries = isset($_POST['no_of_entries']) ? $_POST['no_of_entries']:"";
					$used_entries = isset($_POST['used_entries']) ? $_POST['used_entries']:"";
					$valid_up_to = isset($_POST['valid_up_to']) ? $_POST['valid_up_to']:"";
					$status = isset($_POST['status']) ? $_POST['status']:"";
					
					$sql = "INSERT into ".$qrCode_table_name." (userdetails_id, url, no_of_entries, used_entries, valid_up_to, status) values(".$userdetails_id.", '".$url."', '".$no_of_entries."', '".$used_entries."', '".$valid_up_to."', '".$status."');";
					
					// prepare query statement
					$stmt = $db->prepare($sql);
			 
					// execute query
					if($stmt->execute()){
						$qrcode_id = $db->lastInsertId();	
					
						$code="1";
						$msg="Success";
					
						$data= array(
						"response_code" => $code,
						"status" => $msg,
						"data" =>$product_item=array(
						"qrcode_id" => $qrcode_id,
						"userdetails_id" => $userdetails_id,
						"url" => $url,
						"no_of_entries" => $no_of_entries,
						"used_entries" => $used_entries,
						"valid_up_to" => $valid_up_to,
						"status" => $status)
						);

					} else {
						$code="0";
						$msg="User already has existing QR code!";
			
						$data= array(
							"response_code" => $code,
							"status" => $msg
						);

					}
					
				} else {
					$code="0";
					$msg="Unauthorized, QR code not generated!";
			
					$data= array(
						"response_code" => $code,
						"status" => $msg
					);

				}
		    }
			
		} else {
			
			$code="0";
			$msg="Unidentified user!";
			
			$data= array(
		        "response_code" => $code,
				"status" => $msg
			);

		}
	}
}

echo json_encode($data);
?>