<?php
error_reporting(E_ALL);
ini_set("display_errors",0);
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// include database and object files
include_once '../config/database.php';
// instantiate database and product object
$database = new Database(); 
$db = $database->getConnection();
// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;
$auth_table="authdetails";
$userdetails_table="userdetails";
// check connection to database
if (!$db) {
 die("Connection failed: " . mysqli_connect_error());
 exit;
}
$data = array();
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";
$user_id=isset($_POST['user_id']) ? $_POST['user_id']:"";
$owner_id=isset($_POST['owner_id']) ? $_POST['owner_id']:"";
if($_SERVER["REQUEST_METHOD"] != "POST" ||$activ_key == null || $activ_key == ""||$user_id==null||$user_id==""||$owner_id==null||$owner_id==""){
 $code="0";
 $msg = "Incomplete Details!";
 $data= array(
  "response_code" => $code,
  "status" => $msg
 );
} else {
 $activat_query = "SELECT * from " .$auth_table. " WHERE auth_key = '".$activ_key . "'";
 $stmt = $db->prepare($activat_query);
 // execute query
 $stmt->execute();
 $num = $stmt->rowCount();
 if($num!= 1) {
   $code="0";
   $msg="Security check failed!";
   $data = array(
   "response_code" => $code,
   "status" => $msg
  );
 } else{  
	    $groupid_query="select * from ".$userdetails_table." where user_details_id=".$user_id;
		$stmt=$db->prepare($groupid_query);
		$stmt->execute();
		$num=$stmt->rowCount();
			if($num!=1)
			{
				$code="0";
				$msg="Details not found of user!";
				$data = array(
				"response_code" => $code,
				"status" => $msg
               );
			}
            else
			{ if($row=$stmt->fetch(PDO::FETCH_ASSOC)){
				extract($row);
				if($user_group_id==1||$user_group_id==2){
					
					$query="select * from ".$userdetails_table." where user_details_id=".$owner_id." and status='1'";
					$stmt=$db->prepare($query);
				$stmt->execute();
				$num=$stmt->rowCount();
				if($num!=1)
				{
								$code="0";
								$msg="Invalid details";
								$data = array(
								"response_code" => $code,
								"status" => $msg
								);
				}
				else
				{
				 if($row=$stmt->fetch(PDO::FETCH_ASSOC))
				 {
					 extract($row);
					 $owner_group_id=$user_group_id;
					
					if($owner_group_id==2)
					
					{$query="update ".$userdetails_table." set status='0' where user_details_id=".$owner_id;
					if($db->exec($query)) {
						$code="1";
						$msg="Deleted successfully";
						$data = array(
						"response_code" => $code,
						"status" => $msg
						);
					} else {
							$code="0";
							$msg="unable to delete";
							$data = array(
							"response_code" => $code,
							"status" => $msg
							);
						}
					}else{
						
						$code="0";
					$msg="in correct detaiis";
					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
					}
				 }else{
					 
					 
					 $code="0";
					$msg="db connection prob";
					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
				 }
				 
				}
				}
				else
				{
					$code="0";
					$msg="unauthorized user";
					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
    
				}
    
			}else{
				$code="0";
					$msg="Invalid Details";
					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
			}
		}
 }	
}
echo json_encode($data);
?>