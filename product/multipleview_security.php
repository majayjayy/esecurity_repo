<?php
error_reporting(E_ALL);
ini_set("display_errors",0);
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// include database and object files
include_once '../config/database.php';
// instantiate database and product object
$database = new Database(); 
$db = $database->getConnection();
// Table Initialization
$userdetails_table="userdetails";
$auth_table="authdetails";
// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;
// check connection to database
if (!$db) {
 die("Connection failed: " . mysqli_connect_error());
 exit;
}
$data = array();
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";
$user_details_id=isset($_POST['user_details_id']) ? $_POST['user_details_id']:"";
                             
if($_SERVER["REQUEST_METHOD"] != "POST" ||$activ_key == null || $activ_key == ""||$user_details_id==""||$user_details_id==null){
 $code="0";
 $msg = "Incomplete Details!";
 $data= array(
  "response_code" => $code,
  "status" => $msg
 );
} else {
 $activat_query = "SELECT * from " .$auth_table. " WHERE auth_key = '".$activ_key . "'";
 $stmt = $db->prepare($activat_query);
 // execute query
 $stmt->execute();
 $num = $stmt->rowCount();
 if ($num != 1) {
   $code="0";
   $msg="Security check failed!";
   $data = array(
   "response_code" => $code,
   "status" => $msg
  );
 } else {
  
   $query1="SELECT * from ".$userdetails_table." WHERE user_details_id=".$user_details_id;
   $stmt1 = $db->prepare($query1);
   // execute query
   $stmt1->execute();
   $num1 = $stmt1->rowCount();
   echo "  ".$num1;
   if ($num1<=0) {
    $code="0";
    $msg="No user found!";
    $data = array(
    "response_code" => $code,
    "status" => $msg
    );
   } else {
   
    if ($row = $stmt1->fetch(PDO::FETCH_ASSOC)){
     // extract row
     extract($row);
     if($user_group_id==1)
     {
      
      $query="select name from ".$userdetails_table." where user_group_id=3";
	  $result= array();
      $stmt = $db->prepare($query);
      $stmt->execute();
      $num = $stmt->rowCount();
      if($num<=0)
      {
       $code="0";
       $msg="no details found!";
       $data = array(
       "response_code" => $code,
       "status" => $msg
       );
      }
      else{
		  
		$result=$stmt->fetchAll(PDO::FETCH_ASSOC);
		$code="1";
		$data=array(
		"response_code"=>$code,
		"values"=>$result
		);
	}
     
     } 
     else{
      
      $code="0";
     $msg="Not an authorized person!";
     $data = array(
     "response_code" => $code,
     "status" => $msg
     );
      
      
     }
    }else{
     $code="0";
     $msg="Details not found!";
     $data = array(
     "response_code" => $code,
     "status" => $msg
     );
     
    }
   }
 }
}
 echo json_encode($data);
?>   