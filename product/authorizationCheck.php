<?php

error_reporting(E_ALL);
ini_set("display_errors",0);

// required headers
// header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
// header("Access-Control-Allow-Methods: POST");
// header("Access-Control-Max-Age: 3600");
// header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';
include_once '../objects/authdetails.php';
include_once '../objects/utils.php';

// instantiate database and product object
$database = new Database();	
$db = $database->getConnection();

$authdetails = new AuthDetails($db);
$utils = new Utils($db);

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

$data = array();
$authdetails->dev_username = isset($_POST['developer_username']) ? $_POST['developer_username']:"";
$authdetails->dev_pass = isset($_POST['dev_pass']) ? $_POST['dev_pass']:"";
$authdetails->device_ID = isset($_POST['device_serial_ID']) ? $_POST['device_serial_ID']:"";

//start
if ($_SERVER["REQUEST_METHOD"] != "POST" || $authdetails->dev_username == null || $authdetails->dev_username == "" || $authdetails->dev_pass == null || $authdetails->dev_pass == "" || $authdetails->device_ID == null || $authdetails->device_ID == ""){

	$data = $utils->statusDefaultMessage("0", "Invalid parameters!");

} else {

    $data = $authdetails->authorize();

}

echo json_encode($data);
?>