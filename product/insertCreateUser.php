<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

// required headers
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/utils.php';
include_once '../objects/userdetails.php';
include_once '../objects/companies.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$utils = new Utils($db);
$userdetails = new UserDetails($db);
$companies = new Companies($db);

// assign necessary tables
$userdetails_table = "userdetails";
$companies_table = "companies";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// set value to object property
$data = array();
$user_group_id = isset($_POST['user_group_id']) ? $_POST['user_group_id']:"";
$name = isset($_POST['name']) ? $_POST['name']:"";
$email_id = isset($_POST['email_id']) ? $_POST['email_id']:"";
$password = isset($_POST['password']) ? $_POST['password']:"";
$enc_pass = md5($password);
$mobile_no = isset($_POST['mobile_no']) ? $_POST['mobile_no']:"";
$company_name = isset($_POST['company_name']) ? $_POST['company_name']:"";
$vehicle_id = isset($_POST['vehicle_id']) ? $_POST['vehicle_id']:"";
$created_by = isset($_POST['created_by']) ? $_POST['created_by']:null;
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";

//start
if ($user_group_id==Utils::owner_group_id) {

	if($_SERVER["REQUEST_METHOD"] != "POST" || $user_group_id == null || $user_group_id == "" || $name == null || $name == "" || $email_id == null || $email_id == "" || $password == null || $password == "" || $mobile_no == null || $mobile_no == "" || $activ_key == null || $activ_key == "") {

		$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
		echo json_encode($data);
		exit();

	}

	$emailStatus = $utils->validateEmail($email_id);
	$mobileNumberStatus = $utils->validateMobileNumber($mobile_no);
	$securityCheckStatus = $utils->securityCheck($activ_key);
	$usernameDuplicationStatus = $utils->usernameDuplication($email_id);
	$mobileNumberDuplicationStatus = $utils->mobileNumberDuplication($mobile_no);

	if(!$emailStatus->isValid()) {
		$data = $emailStatus->getMessage();
		echo json_encode($data);
		exit();
	} 

	if (!$mobileNumberStatus->isValid()) {
		$data = $mobileNumberStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$securityCheckStatus->isValid()) {
		$data = $securityCheckStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$usernameDuplicationStatus->isValid()) {
		$data = $usernameDuplicationStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$mobileNumberDuplicationStatus->isValid()) {
		$data = $mobileNumberDuplicationStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	$data = $userdetails->insertOwner($user_group_id, $name, $email_id, $enc_pass, $mobile_no, $company_name, $created_by);

} else if ($user_group_id==Utils::security_group_id) {

	if($_SERVER["REQUEST_METHOD"] != "POST" || $user_group_id == null || $user_group_id == "" || $name == null || $name == "" || $password == null || $password == "" || $mobile_no == null || $mobile_no == "" || $activ_key == null || $activ_key == "") {

		$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
		echo json_encode($data);
		exit();

	}

	$mobileNumberStatus = $utils->validateMobileNumber($mobile_no);
	$securityCheckStatus = $utils->securityCheck($activ_key);
	$mobileNumberDuplicationStatus = $utils->mobileNumberDuplication($mobile_no);

	if (!$mobileNumberStatus->isValid()) {
		$data = $mobileNumberStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$securityCheckStatus->isValid()) {
		$data = $securityCheckStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$mobileNumberDuplicationStatus->isValid()) {
		$data = $mobileNumberDuplicationStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	$data = $userdetails->insertSecurity($user_group_id, $name, $enc_pass, $mobile_no, $created_by);

} else if ($user_group_id==Utils::visitor_group_id) {

	if($_SERVER["REQUEST_METHOD"] != "POST" || $user_group_id == null || $user_group_id == "" || $name == null || $name == "" || $email_id == null || $email_id == "" || $mobile_no == null || $mobile_no == "" || $vehicle_id == null || $vehicle_id == "" || $activ_key == null || $activ_key == "") {

		$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
		echo json_encode($data);
		exit();

	}

	$emailStatus = $utils->validateEmail($email_id);
	$mobileNumberStatus = $utils->validateMobileNumber($mobile_no);
	$securityCheckStatus = $utils->securityCheck($activ_key);
	$usernameDuplicationStatus = $utils->usernameDuplication($email_id);
	$mobileNumberDuplicationStatus = $utils->mobileNumberDuplication($mobile_no);

	if(!$emailStatus->isValid()) {
		$data = $emailStatus->getMessage();
		echo json_encode($data);
		exit();
	} 

	if (!$mobileNumberStatus->isValid()) {
		$data = $mobileNumberStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$securityCheckStatus->isValid()) {
		$data = $securityCheckStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$usernameDuplicationStatus->isValid()) {
		$data = $usernameDuplicationStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	if (!$mobileNumberDuplicationStatus->isValid()) {
		$data = $mobileNumberDuplicationStatus->getMessage();
		echo json_encode($data);
		exit();
	}

	$data = $userdetails->insertVisitor($user_group_id, $name, $email_id, $mobile_no, $vehicle_id, $created_by);

} else {

	$data = $utils->statusDefaultMessage("0", "Something went wrong!");

}

//end
echo json_encode($data);

?>