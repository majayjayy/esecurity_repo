<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

// required headers
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");

// include database and object files
include_once '../config/database.php';
include_once '../objects/utils.php';
include_once '../objects/userdetails.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$utils = new Utils($db);
$userdetails = new UserDetails($db);

// assign necessary tables
$userdetails_table = "userdetails";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// set value to object property
$data = array();
$userdetails_id = isset($_POST['userdetails_id']) ? $_POST['userdetails_id']:"";
$view_user_id = isset($_POST['view_user_id']) ? $_POST['view_user_id']:"";
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";

//start
if($_SERVER["REQUEST_METHOD"] != "POST" || $userdetails_id == null || $userdetails_id == "" || $activ_key == null || $activ_key == "") {

	$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
	echo json_encode($data);
	exit();

} 

$securityCheckStatus = $utils->securityCheck($activ_key);

if (!$securityCheckStatus->isValid()) {
	$data = $securityCheckStatus->getMessage();
	echo json_encode($data);
	exit();
}

if ($view_user_id == null || $view_user_id == "") {
	
	$user_query = "SELECT * from " .$userdetails_table. " WHERE user_details_id = '".$userdetails_id . "'";
	$stmt = $db->prepare($user_query);
	$stmt->execute();
	$num = $stmt->rowCount();

	if ($num==1) {
		
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

	        extract($row);

	        if ($user_group_id == Utils::owner_group_id) {

	        	$newName = isset($_POST['name']) ? $_POST['name']:$name;
				$newEmail_id = $email_id;
				$newMobile_no = isset($_POST['mobile_no']) ? $_POST['mobile_no']:$mobile_no;
				$newVehicle_id = $vehicle_id;

				$mobileNumberStatus = $utils->validateMobileNumber($newMobile_no);
				$mobileNumberDuplicationStatus = $utils->mobileNumberDuplication($newMobile_no);

				if (!$mobileNumberStatus->isValid()) {
					$data = $mobileNumberStatus->getMessage();
					echo json_encode($data);
					exit();
				}

				if (!$mobileNumberDuplicationStatus->isValid()) {
					$data = $mobileNumberDuplicationStatus->getMessage();
					echo json_encode($data);
					exit();
				}

	        	$data = $userdetails->updateUser($userdetails_id, $newName, $newEmail_id, $newMobile_no, $newVehicle_id);

	        } else if ($user_group_id == Utils::security_group_id) {
	        	
	        	$newName = isset($_POST['name']) ? $_POST['name']:$name;
				$newEmail_id = $email_id;
				$newMobile_no = $mobile_no;
				$newVehicle_id = $vehicle_id;

				$data = $userdetails->updateUser($userdetails_id, $newName, $newEmail_id, $newMobile_no, $newVehicle_id);

	        } else {

	        	$data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");

	        }

	    }

	} else {

		$data = $utils->statusDefaultMessage("0", "Invalid user!");

	}

} else if ($view_user_id != null || $view_user_id != "") {
	
	$owner_query = "SELECT * from " .$userdetails_table. " WHERE user_details_id = '".$userdetails_id . "'";
	$stmt = $db->prepare($owner_query);
	$stmt->execute();
	$num = $stmt->rowCount();
	$owner_details_id = null;
	$owner_group_id = null;
	$owner_company_id = null;
	if ($num==1) {
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	        extract($row);
	        $owner_details_id = $user_details_id;
	        $owner_group_id = $user_group_id;
	        $owner_company_id = $company_id;
	    }
	}

	$user_query = "SELECT * from " .$userdetails_table. " WHERE user_details_id = '".$view_user_id . "'";
	$stmt = $db->prepare($user_query);
	$stmt->execute();
	$num = $stmt->rowCount();

	if ($num==1) {
		
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

	        extract($row);

	        if ($owner_group_id == Utils::admin_group_id || ($owner_group_id == Utils::owner_group_id && ($owner_details_id == $created_by || $owner_company_id == $company_id))) {

				$newName = isset($_POST['name']) ? $_POST['name']:$name;
				$newEmail_id = isset($_POST['email_id']) ? $_POST['email_id']:$email_id;
				$newMobile_no = isset($_POST['mobile_no']) ? $_POST['mobile_no']:$mobile_no;
				$newVehicle_id = isset($_POST['vehicle_id']) ? $_POST['vehicle_id']:$vehicle_id;

				$emailStatus = $utils->validateEmail($newEmail_id);
				$mobileNumberStatus = $utils->validateMobileNumber($newMobile_no);
				$usernameDuplicationStatus = $utils->usernameDuplication($newEmail_id);
				$mobileNumberDuplicationStatus = $utils->mobileNumberDuplication($newMobile_no);

				if(!$emailStatus->isValid()) {
					$data = $emailStatus->getMessage();
					echo json_encode($data);
					exit();
				} 

				if (!$mobileNumberStatus->isValid()) {
					$data = $mobileNumberStatus->getMessage();
					echo json_encode($data);
					exit();
				}

				if (!$usernameDuplicationStatus->isValid()) {
					$data = $usernameDuplicationStatus->getMessage();
					echo json_encode($data);
					exit();
				}

				if (!$mobileNumberDuplicationStatus->isValid()) {
					$data = $mobileNumberDuplicationStatus->getMessage();
					echo json_encode($data);
					exit();
				}

		        $data = $userdetails->updateUser($view_user_id, $newName, $newEmail_id, $newMobile_no, $newVehicle_id);

	        } else {

	        	$data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");

	        }

	    }

	} else {

		$data = $utils->statusDefaultMessage("0", "Invalid user!");

	}

} else {

	$data = $utils->statusDefaultMessage("0", "Something went wrong!");

}

//end
echo json_encode($data);

?>