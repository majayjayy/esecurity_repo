<?php
error_reporting(E_ALL);
ini_set("display_errors",0);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../config/database.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

// assign necessary tables
$userdetails_table = "userdetails";
$auth_table = "authdetails";
$companies_table="companies";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// check connection to database
if (!$db) {
	die("Connection failed: " . mysqli_connect_error());
	exit;
}

$data = array();
$user_id = isset($_POST['user_id']) ? $_POST['user_id']:"";
$owner_id = isset($_POST['owner_id']) ? $_POST['owner_id']:"";
$name = isset($_POST['name']) ? $_POST['name']:"";
$mobile_no = isset($_POST['mobile_no']) ? $_POST['mobile_no']:"";
$company_name=isset($_POST['company_name'])?$_POST['company_name']:"";
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";



//start
if($_SERVER["REQUEST_METHOD"] != "POST" || $activ_key == null || $activ_key == ""||$user_id==null||$user_id==""||$owner_id==""||$owner_id==null||$name==""||$name==null||$mobile_no==""||$mobile_no==null||$company_name==""||$company_name==null){

	$code="0";
	$msg = "Invalid parameters!"; 

	$data= array(
		"response_code" => $code,
		"status" => $msg
	);

} else {

	// Security check with Activation key
	$activat_query = "SELECT * from " .$auth_table. " WHERE auth_key = '".$activ_key . "'";

	// prepare query statement
	$stmt = $db->prepare($activat_query);
	// execute query
	$stmt->execute();
	$num = $stmt->rowCount();

	if ($num != 1) {

		$code="0";
		$msg="Security check failed!";

		$data = array(
			"response_code" => $code,
			"status" => $msg
		);

	} else {
		
			$groupid_query="select user_group_id from ".$userdetails_table." where user_details_id=".$user_id;
			$stmt=$db->prepare($groupid_query);
			$stmt->execute();
			$num=$stmt->rowCount();
			if($num!=1)
			{
				$code="0";
					$msg="Details not found!";

					$data = array(
					"response_code" => $code,
					"status" => $msg
					);
			}
		
			else
			{
				if($row=$stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row);
					if(($user_group_id==1)||($user_group_id==2&&$user_id==$owner_id))
					{	
						
						$query="select user_group_id from ".$userdetails_table." where user_details_id=".$owner_id;
						$stmt = $db->prepare($query);
						$stmt->execute();
						$num = $stmt->rowCount();

						if ($num != 1) {

							$code="0";
							$msg="No user found!";

							$data = array(
							"response_code" => $code,
							"status" => $msg
							);

						} else {
			
							if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
								extract($row);
								$owner_group_id=$user_group_id;
			  
								if($owner_group_id==2)
								{	
						
									$company_name_query = "SELECT * from " .$companies_table. " WHERE company_name = '".$company_name ."'";
									$stmt = $db->prepare($company_name_query);
									$stmt->execute();
									$num = $stmt->rowCount();
			
									if($num == 1) {
									if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
									extract($row);
									$sql_sub = "UPDATE ".$userdetails_table." SET ";
									$sql_sub = $sql_sub. "name='".$name."', mobile_no='".$mobile_no."',company_id=".$company_id;
									$sql_sub=$sql_sub. " WHERE user_details_id=".$owner_id.";";
									$sql=$sql_sub;
									if($db->exec($sql)){

										$code="1";
										$msg="Details updated successfully!";

										$data= array(
										"response_code" => $code,
										"status" => $msg
										);
									} else {

										$code="0";
									$msg="Details could not be updated!";
										$data= array(
										"response_code" => $code,
										"status" => $msg
											);
									}	
								}	
								else
							{
								$code="0";
							$msg="Details not found!";

							$data= array(
							"response_code" => $code,
							"status" => $msg
							);
							}
						}
					else{
						$sql = "INSERT into ".$companies_table." (company_name) values('".$company_name."');";
						if($db->exec($sql)) {
						$last_id = $db->lastInsertId();
						$sql_sub = "UPDATE ".$userdetails_table." SET ";
						$sql_sub = $sql_sub. "name='".$name."', mobile_no=".$mobile_no.",company_id=".$last_id;
						$sql_sub=$sql_sub. " WHERE user_details_id=".$owner_id.";";
						$sql=$sql_sub;
						if($db->exec($sql)){

							$code="1";
							$msg="Details updated successfully!";

							$data = array(
								"response_code" => $code,
								"status" => $msg
							   );

						} else {
		
							$code="0";
							$msg="Details couldn't be updated!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);

						}
				
						}
						else
						{
							$code="0";
							$msg="Details couldn't be updated!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
						}
					}
				}
				else
				{
							$code="0";
							$msg="Not an authorized user!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
				}
			}
			else{
							$code="0";
							$msg="Details not found!";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
			}
			}
			}else{
				
							$code="0";
							$msg="Not authorized user! ";

										$data = array(
											"response_code" => $code,
											"status" => $msg
										);
				
			}}
			else{
				
				$code="0";
				$msg="Not authorized user!";

				$data = array(
				"response_code" => $code,
				"status" => $msg
				);
				
			}
		}
		}
		
}
echo json_encode($data);
//end
?>