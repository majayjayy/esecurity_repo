<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

// required headers
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");

// include database and object files
include_once '../config/database.php';
include_once '../objects/utils.php';
include_once '../objects/userdetails.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$utils = new Utils($db);
$userdetails = new UserDetails($db);

// assign necessary tables
$userdetails_table = "userdetails";

// get posted data
$json = json_decode(file_get_contents("php://input"));
$_POST = (array)$json;

// set value to object property
$data = array();
$userdetails_id = isset($_POST['userdetails_id']) ? $_POST['userdetails_id']:"";
$view_group_id = isset($_POST['view_group_id']) ? $_POST['view_group_id']:"";
$activ_key = isset($_POST['activ_key']) ? $_POST['activ_key']:"";

//start
if($_SERVER["REQUEST_METHOD"] != "POST" || $userdetails_id == null || $userdetails_id == "" || $view_group_id == null || $view_group_id == "" || $activ_key == null || $activ_key == "") {

	$data = $utils->statusDefaultMessage("0", "Invalid parameters!");
	echo json_encode($data);
	exit();

} 

$securityCheckStatus = $utils->securityCheck($activ_key);

if (!$securityCheckStatus->isValid()) {
	$data = $securityCheckStatus->getMessage();
	echo json_encode($data);
	exit();
}
	
$user_query = "SELECT * from " .$userdetails_table. " WHERE user_details_id = '".$userdetails_id . "'";
$stmt = $db->prepare($user_query);
$stmt->execute();
$num = $stmt->rowCount();

if ($num==1) {
	
	if ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        extract($row);

        if ($user_group_id == Utils::admin_group_id) {

        	$data = $userdetails->viewUserGroup($user_group_id, $view_group_id, null, null);

        } elseif ($user_group_id == Utils::owner_group_id) {
        	
        	$data = $userdetails->viewUserGroup($user_group_id, $view_group_id, $userdetails_id, $company_id);

        } else {

        	$data = $utils->statusDefaultMessage("0", "Unauthorized to perform action!");

        }

    }

} else {

	$data = $utils->statusDefaultMessage("0", "Invalid user!");

}

//end
echo json_encode($data);

?>